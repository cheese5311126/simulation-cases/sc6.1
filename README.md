# Urgent high-resolution 3D multiphase flow simulation of phreatic eruptions at Vulcano

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/INGV.png?inline=false" width="50">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/Cineca.png?inline=false" width="90">

**Codes:** [OpenPDAC](https://gitlab.com/cheese5311126/codes/openpdac/openpdac-11)

**Target EuroHPC Architectures:** LUMI-C

**Type:** Urgent computing

## Description

Phreatic eruptions are surface manifestations of the instability of a
volcano-hydrothermal system. They are often associated with the increase of
pressure and temperature in a shallow hydrothermal system fed by heat and gas
flux from a deeper magma source. The sudden decompression of the
hydrothermal fluids in the shallow, porous rocks might cause the fragmentation
of the rocks and the ejection of a mixture of gases (mostly water vapour), fine
(sub-millimeter) to coarse (mm) ash and lapilli (cm-sized) material, and large
(decimeter-sized) bombs. Phreatic explosions are relatively small in size (usually <10⁵ m3 of ejected materials) compared to magmatic eruptions, but their impact
can be significant, especially on populated volcanic regions and on touristic
areas, such as volcanic islands. At Vulcano (Aeolian islands, Italy), the recent
2021 hydrothermal unrest has raised awareness of the need of assessing the
hazards associated with potential phreatic explosions. Fluid dynamics modelling
is a fundamental tool to characterise phreatic eruption scenarios (i.e., identifying
the controlling initial parameters and instability conditions), to reconstruct past
events, to define areas more likely impacted by the eruptive products, and to
communicate volcanic risk. So far, the high computational cost of
three-dimensional, multiphase flow numerical models have hindered the
possibility of simulating a full eruptive scenario at relevant spatial scales (i.e.,
sub-meter scale) in a short time-frame (~ hours) compatible with the decision
making process during a volcanic crisis. The advent of exascale computing and
the increased availability of open-source computational fluid dynamic codes that
can scale up to thousands of CPU cores makes it nowadays possible to address
this challenge and to test, for the first time, Urgent Computing workflows for
anticipating the impact of volcanic eruptions.

<img src="Sc6.1.png" width="800">

**Figure 3.9.1.** Graphical abstract of SC6.1. Onset of a phreatic explosion at Vulcano
(Aeolian archipelago, Southern Tyrrhenian Sea, Italy). 3D simulation performed with
OpenPDAC flagship code.

## Expected results

Functional requirements will be defined with the potential stakeholders at the beginning of the project. As
an indication, the goal is to achieve a spatial resolution at ground <1m, domain size ~5×5×5 km3 (covering the whole island)
potentially requiring >108 elements and >109 degrees of freedom solved on O(104) cores. The time-to-solution for a single
explosive event lasting 5 to 10 minutes should remain below about 10-15 min. Outputs will include maps of maximum
temperature, dynamic pressure, ash concentration, ballistic fall landing time, density and energy. Video animations will be
generated for dissemination and outreach, including the organization of a UC “live” exercise involving potential stakeholders
(Italian Civil Protection, Lipari Municipality, DG-ECHO). The case has potential extension to other volcanic areas (e.g.,
Guadeloupe, Martinique, Solfatara) through Transnational Access.